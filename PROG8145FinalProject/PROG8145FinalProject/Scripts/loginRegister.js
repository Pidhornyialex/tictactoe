﻿var logged;
var displayName;

function dataExchange(params, url, resultFunc) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            resultFunc(this.responseText);
        }
    };

    xhttp.send(params);
}

function btnLoginClick() {
    var vlogin = document.getElementById('tbLogin').value;
    var password = document.getElementById('tbPassword').value;
    login(vlogin, password);
};

function btnRegisterClick() {
    register(document.getElementById('tbRLogin').value, document.getElementById('tbRPassword').value);
};

function login(login, password) {
    dataExchange("login=" + login + "&password=" + password, "User/Login", loginResponse);
};

function register(login, password) {
    dataExchange("login=" + login + "&password=" + password, "User/Register", registerResponse);
};

function registerResponse(response) {
    if (response.charAt(0) == "S") {
        logged = true;
        displayName = response.substring(2);
        $('#registered').show();

    }
    else {
        logged = false;
        displayName = "";
        $('#badData').show();
    }
};

function loginResponse(response) {
    if (response.charAt(0) == "S") {
        logged = true;
        displayName = response.substring(2);
        userName = displayName;
        window.location.replace("Game");
    }
    else {
        logged = false;
        displayName = "";
        $('#incorrectLogin').show();
    }
};