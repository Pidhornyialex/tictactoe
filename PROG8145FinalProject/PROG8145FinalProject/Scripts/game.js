﻿var mySign = 1;
var cells = [];
var code = -1;
var boardSide = 0;
var DEFAULT_INTERVAL = 1000;
var recievedResponse = true;
var interval;
var againstAI = false;

function AIopponentClick() {
    if ($('#tbOpponent').is(':visible')) {
        $("#tbOpponent").addClass('hidden');
        againstAI = true;
    }
};

function OpponentClick() {
    if ($('#tbOpponent').is(':hidden')) {
        $("#tbOpponent").removeClass('hidden');
        againstAI = false;
    }
};

function update() {
    if (recievedResponse && !victory) {
        dataExchange("Code=" + code, "UpdateState", updateResp);
        recievedResponse = false;
    }

};
var victory = false;
function victoryF(c) {
    clearInterval(interval);
    if (c == mySign) {
        document.getElementById("playP").innerHTML = "You win!";
    }
    else if (c == 0) {
        document.getElementById("playP").innerHTML = "It's a draw!";
    }
    else {
        document.getElementById("playP").innerHTML = "You loose :(";
    }
};
function checkForVictory() {
    var numberRequired = Math.floor(3 + (boardSide - 3) * 0.1);
    var vertical1 = 0;
    var horizontal1 = 0;
    var vertical2 = 0;
    var horizontal2 = 0;

    var mainDiag1 = 0;
    var secDiag1 = 0;
    var mainDiag2 = 0;
    var secDiag2 = 0;

    var draw = true;
    for (var i = 0; i < cells.length; i++) {
        if (cells[i] == 0) {
            draw = false;
            break;
        }
    }
    if (draw && cells.length > 0)
        victoryF(0);

    for (var i = 0; i < boardSide; i++) {
        for (var j = 0; j < boardSide; j++) {
            if (cells[j + i * boardSide] == 1) {
                horizontal1++;
                horizontal2 = 0;
            }
            else if (cells[j + i * boardSide] == 2) {
                horizontal1 = 0;
                horizontal2++;
            }
            else {
                horizontal1 = 0;
                horizontal2 = 0;
            }

            if (cells[i + j * boardSide] == 1) {
                vertical1++;
                vertical2 = 0;
            }
            else if (cells[i + j * boardSide] == 2) {
                vertical1 = 0;
                vertical2++;
            }
            else {
                vertical1 = 0;
                vertical2 = 0;
            }

            if (horizontal1 >= numberRequired) {
                victoryF(1);
            }
            else if (horizontal2 >= numberRequired) {
                victoryF(2);
            }
            else if (vertical1 >= numberRequired) {
                victoryF(1);
            }
            else if (vertical2 >= numberRequired) {
                victoryF(2);
            }

        }
        horizontal1 = 0;
        horizontal2 = 0;
        vertical1 = 0;
        vertical2 = 0;
    }

    for (var j = boardSide - numberRequired; j >= 0; j--) {
        for (var i = 0; i < boardSide; i++) {
            if (cells[(j + i) * boardSide + i] == 1) {
                mainDiag1++;
                mainDiag2 = 0;
            }
            else if (cells[(j + i) * boardSide + i] == 2) {
                mainDiag1 = 0;
                mainDiag2++;
            }
            if (mainDiag1 >= numberRequired) {
                victoryF(1);
            }
            else if (mainDiag2 >= numberRequired) {
                victoryF(2);
            }
        }
        mainDiag1 = 0;
        mainDiag2 = 0;
        for (var i = 0; i < boardSide; i++) {
            if (cells[(i) * boardSide + i + j] == 1) {
                mainDiag1++;
                mainDiag2 = 0;
            }
            else if (cells[(i) * boardSide + i + j] == 2) {
                mainDiag1 = 0;
                mainDiag2++;
            }
            if (mainDiag1 >= numberRequired) {
                victoryF(1);
            }
            else if (mainDiag2 >= numberRequired) {
                victoryF(2);
            }
        }
        mainDiag1 = 0;
        mainDiag2 = 0;
    }

    var vj = boardSide - numberRequired;
    for (var j = vj; j >= -vj; j--) {
        for (var i = 0; i < boardSide; i++) {
            if (cells[(numberRequired - j - i - 1) * boardSide + i] == 1) {
                secDiag1++;
                secDiag2 = 0;
            }
            else if (cells[(boardSide - j - i - 1) * numberRequired + i] == 2) {
                secDiag1 = 0;
                secDiag2++;
            }
            if (secDiag1 >= numberRequired) {
                victoryF(1);
            }
            else if (secDiag2 >= numberRequired) {
                victoryF(2);
            }
        }
        secDiag1 = 0;
        secDiag2 = 0;
    }
    
};

function updateVisual() {
    checkForVictory();
    var docid = "";
    for (var i = 0; i < cells.length; i++) {
        docid = 'cell_' + i % boardSide + "_" + Math.floor(i / boardSide);
        if (cells[i] == 0 && document.getElementById(docid).innerHTML != "")
            document.getElementById(docid).innerHTML = "";
        if (cells[i] == 1 && document.getElementById(docid).innerHTML != "X")
            document.getElementById(docid).innerHTML = "X";
        if (cells[i] == 2 && document.getElementById(docid).innerHTML != "O")
            document.getElementById(docid).innerHTML = "O";
    }
};

function startNewGame(side, opponent) {
    console.log(window.location);
    dataExchange("opponent=" + opponent + "&side=" + side, "/Game/StartGame", startNewGameResp);

};

function joinGame(id) {
    dataExchange("gameid=" + id, "/Game/JoinGame", joinGameResp);
};

function postResp(response) {
    var obj = JSON.parse(response);
    if (obj.approved)
        code = obj.code;
};
var obj;
function updateResp(response) {
    recievedResponse = true;
    obj = JSON.parse(response);
    if (obj.isUpdated) {
        code = obj.code;
        var curr = 0;
        for (var i = 0; i < obj.board.length; i++) {
            curr = parseInt(obj.board[i]);
            if (cells.length > i)
                cells[i] = curr;
            else
                cells.push(curr);

        }

    }
    updateVisual();
};

function joinGameResp(response) {
    obj = JSON.parse(response);
    if (obj.success == true) {
        window.location.replace("/Game/Play");
    }
};

function startNewGameResp(response) {
    obj = JSON.parse(response);
    if (obj.success == true) {
        window.location.replace("/Game/Play");
    }
};

function CellClick(x, y) {
    //registerMove
    if (document.getElementById("opponentInvite").innerHTML != "")
        document.getElementById("opponentInvite").innerHTML = "";
    if (!victory) {
        if (cells[y * boardSide + x] == 0 && code % 2 == mySign % 2) {
            cells[y * boardSide + x] = mySign;
            var board = "";
            for (var i = 0; i < cells.length; i++) {
                board += cells[i];
            }
            updateVisual();
            dataExchange("board=" + board + "&code=" + code, "PostChanges", postResp);
        }
    }
};

var btnNewClickCounter = 0;
var btnJoinClickCounter = 0;

function btnNewClick() {
    var side;
    if (document.getElementById('tbSide').value == "")
        side = "3";
    else
        side = document.getElementById('tbSide').value;
    var opponent;
    if (againstAI || document.getElementById('tbOpponent').value == "")
        opponent = "AI";
    else
        opponent = document.getElementById('tbOpponent').value;
    if ($('#tbSide').is(':hidden')) {
        $("#tbSide").removeClass('hidden');
    }
    if ($('#opponentChoise').is(':hidden')) {
        $("#opponentChoise").removeClass('hidden');
    }
    if ($('#tbGame').is(':visible')) {
        $("#tbGame").addClass('hidden');
    }
    if (btnNewClickCounter >= 1) {
        startNewGame(side, opponent);
    }
    btnNewClickCounter++;
    btnJoinClickCounter = 0;
};

function btnJoinClick() {
    var game = document.getElementById('tbGame').value;
    if ($('#tbSide').is(':visible')) {
        $("#tbSide").addClass('hidden');
    }
    if ($('#opponentChoise').is(':visible')) {
        $("#opponentChoise").addClass('hidden');
    }
    if ($('#tbGame').is(':hidden')) {
        $("#tbGame").removeClass('hidden');
    }
    if (btnJoinClickCounter >= 1) {
        joinGame(game);
    }
    btnNewClickCounter = 0;
    btnJoinClickCounter++;
};

function btnEndClick() {

};