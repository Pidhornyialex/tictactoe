﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROG8145FinalProject.Models
{
    public class JoinGameRequest
    {
        public int gameid { get; set; }
    }
    public class JoinGameResponse
    {
        public bool success { get; set; }
        public string board { get; set; }
        public int code { get; set; }
    }
    public class PostChangesRequest
    {
        public string board { get; set; }
        public int code { get; set; }
    }
    public class PostChangesResponse
    {
        public bool approved = false;
        public int code = -1;
    }
    public class StartGameRequest
    {
        public string opponent { get; set; }
        public int side { get; set; }
    }
    public class StartGameResponse
    {
        public bool success { get; set; }
        public int code { get; set; }
    }
    public class UpdateRequest
    {
        public int Code { get; set; }
    }
    public class UpdateResponse
    {
        public bool isUpdated = false;
        public string board = "";
        public int code = -1;
    }
}