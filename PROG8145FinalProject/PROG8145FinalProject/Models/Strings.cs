﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROG8145FinalProject.Controllers
{
    public class Strings
    {
        public static string USERNAME = "userName";
        public static string GAMEID = "gameID";
        public static string BOARDSIDE = "boardside";
        public static string MYSIGN = "mySign";
        public static string OPPSIGN = "oppSign";
    }
}