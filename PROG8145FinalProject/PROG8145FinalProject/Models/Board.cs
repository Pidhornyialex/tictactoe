﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROG8145FinalProject.Models
{
    public class Board
    {
        private int[] cells;
        int side;
        public Board(int side)
        {
            this.side = side;
            cells = new int[side * side];
            for (int i = 0; i < cells.Length; i++)
                cells[i] = 0;
        }
        public Board(string str)
        {
            cells = new int[str.Length];
            for (int i = 0; i < cells.Length; i++)
            {
                cells[i] = Convert.ToInt32(str[i]);
            }
            side = (int)(Math.Sqrt(cells.Length));
        }
        public string toIntArray()
        {
            string res = "";
            for (int i = 0; i < cells.Length; i++)
            {
                res += cells[i].ToString();
            }
            return res;
        }
    }
}
