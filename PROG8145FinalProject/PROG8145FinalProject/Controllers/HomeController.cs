﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROG8145FinalProject.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Session["logged"] = "";
            return View();
        }
    }
}