﻿using PROG8145FinalProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROG8145FinalProject.Controllers
{
    public class UserController : Controller
    {
        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index", "Home");

        }
        [HttpPost]
        public ActionResult Register(User u)
        {
            if (UserValid(u) == UserStatus.LoginIncorrect)
            {
                AddUser(u);
                return Content("S:" + u.login);
            }
            else
            {
                return Content("F");
            }

        }

        [HttpPost]
        public ActionResult Login(User u)
        {
            string resp = "";
            UserStatus us = UserValid(u);
            if (us == UserStatus.LoginIncorrect)
            {
                resp = "F:Incorrect Login";
                Session[Strings.USERNAME] = "";
            }
            else if (us == UserStatus.PasswordIncorrect)
            {
                resp = "F:Incorrect Password";
                Session[Strings.USERNAME] = "";
            }
            else if (us == UserStatus.Correct)
            {
                resp = "S:" + u.login;
                Session[Strings.USERNAME] = u.login;
            }
            return Content(resp);
        }

        private bool AddUser(User u)
        {
            TicTacToeDatabaseEntities userDB = new TicTacToeDatabaseEntities();
            userDB.Users.Add(u);
            try
            {
                userDB.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        private UserStatus UserValid(User u)
        {
            TicTacToeDatabaseEntities userDB = new TicTacToeDatabaseEntities();
            var Users = userDB.Users.ToList();
            foreach (User f in Users)
            {
                f.login = f.login.Trim();
                f.password = f.password.Trim();
                if (f.login == u.login)
                {
                    if (f.password == u.password)
                    {
                        return UserStatus.Correct;
                    }
                    else
                    {
                        return UserStatus.PasswordIncorrect;
                    }
                }
                else
                {

                }
            }
            return UserStatus.LoginIncorrect;
        }

        private enum UserStatus { Correct, PasswordIncorrect, LoginIncorrect };
    }
}