﻿using PROG8145FinalProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROG8145FinalProject.Controllers
{
    public class GameController : Controller
    {
        // GET: Game
        public ActionResult Index()
        {
            if (Session[Strings.USERNAME] == null || (string)Session[Strings.USERNAME] == "")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewData[Strings.USERNAME] = Session[Strings.USERNAME];
                return View();
            }
        }
        public ActionResult Play()
        {
            if (Session[Strings.USERNAME] == null || (string)Session[Strings.USERNAME] == "" ||
                Session[Strings.GAMEID] == null || (int)Session[Strings.GAMEID] == -1)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TicTacToeDatabaseEntities gamesDB = new TicTacToeDatabaseEntities();
                List<Game> games = gamesDB.Games.ToList();
                Game lastGame = (from game in games where game.Id == (int)(Session[Strings.GAMEID]) select game).FirstOrDefault();
                ViewData[Strings.BOARDSIDE] = Math.Sqrt(lastGame.board.Trim().Length);
                ViewData[Strings.MYSIGN] = (lastGame.player1.Trim() == (string)Session[Strings.USERNAME]) ? 1 : 2;
                if (lastGame.player2.Trim() == "AI" || lastGame.player2.Trim() == (string)Session[Strings.USERNAME])
                    ViewData["gameid"] = "";
                else
                    ViewData["gameid"] = "Please tell " + lastGame.player2.Trim() + " that the game id is: " + lastGame.Id;
                return View();
            }
        }

        public ActionResult EndGame()
        {
            if (Session[Strings.USERNAME] == null || (string)Session[Strings.USERNAME] == "")
            {
                return RedirectToAction("Index", "Home");
            }
          
            if (Session[Strings.GAMEID] == null || (int)Session[Strings.GAMEID] == -1)
            {
                return RedirectToAction("Index", "Game");
            }
            else
            {
                Session[Strings.GAMEID] = -1;
                return RedirectToAction("Index", "Game");
            }

        }

        [HttpPost]
        public JsonResult JoinGame(JoinGameRequest req)
        {
            TicTacToeDatabaseEntities gamesDB = new TicTacToeDatabaseEntities();
            var gamesList = (from game in gamesDB.Games.ToList()
                             where game.Id == req.gameid &&
                             (game.player2.Trim() == (string)Session[Strings.USERNAME])
                             select game);
            JoinGameResponse ur = new JoinGameResponse();
            ur.success = false;
            if (gamesList.Count() > 0)
            {
                Game game = gamesList.FirstOrDefault();
                Session[Strings.GAMEID] = game.Id;
                ur.board = game.board;
                ur.success = true;
                ur.code = game.code;

            }
            return Json(ur);
        }

        [HttpPost]
        public JsonResult StartGame(StartGameRequest req)
        {
            TicTacToeDatabaseEntities gamesDB = new TicTacToeDatabaseEntities();
            List<Game> games = gamesDB.Games.ToList();
            Game newSession = new Game();
            newSession.Id = games.Last().Id + 1;
            newSession.player1 = (string)(Session[Strings.USERNAME]);
            newSession.player2 = req.opponent;
            newSession.code = 1;
            Board b = new Board(req.side);
            newSession.board = b.toIntArray();
            gamesDB.Games.Add(newSession);
            gamesDB.SaveChanges();
            StartGameResponse resp = new StartGameResponse();
            Session[Strings.GAMEID] = newSession.Id;
            resp.code = newSession.code;
            resp.success = true;
            return Json(resp);
            //((Board)Session["board"]).RegisterMove(m);
            //Move m2 = ((Board)Session["board"]).RequestMove();
            //if(m2 != null)
            //    ((Board)Session["board"]).RegisterMove(m2);
            //return Content("S|" + m2.x + "|" + m2.y + "|" + ((Board)Session["board"]).getVictory());
        }

        [HttpPost]
        public JsonResult UpdateState(UpdateRequest ureq)
        {
            TicTacToeDatabaseEntities gamesDB = new TicTacToeDatabaseEntities();
            List<Game> games = gamesDB.Games.ToList();
            Game lastGame = (from game in games where game.Id == (int)(Session[Strings.GAMEID]) select game).FirstOrDefault();
            UpdateResponse ur = new UpdateResponse();
            if ((lastGame.player1.Trim() == (string)(Session[Strings.USERNAME]) && lastGame.code % 2 == 1) ||
                (lastGame.player2.Trim() == (string)(Session[Strings.USERNAME]) && lastGame.code % 2 == 0) &&
                ureq.Code < lastGame.code)
            {
                ur.isUpdated = true;
            }
            ur.board = lastGame.board.Trim();
            ur.code = lastGame.code;
            return Json(ur);
            //TODO
            //((Board)Session["board"]).RegisterMove(m);
            //Move m2 = ((Board)Session["board"]).RequestMove();
            //if(m2 != null)
            //    ((Board)Session["board"]).RegisterMove(m2);
            //return Content("S|" + m2.x + "|" + m2.y + "|" + ((Board)Session["board"]).getVictory());
        }
        [HttpPost]
        public JsonResult PostChanges(PostChangesRequest postChangesReq)
        {
            TicTacToeDatabaseEntities gamesDB = new TicTacToeDatabaseEntities();
            List<Game> games = gamesDB.Games.ToList();
            Game nowSaved = (from game in games where game.Id == (int)(Session[Strings.GAMEID]) select game).FirstOrDefault();
            Game updateGame = new Game();
            updateGame.Id = nowSaved.Id;
            updateGame.player1 = nowSaved.player1;
            updateGame.player2 = nowSaved.player2;

            PostChangesResponse resp = new PostChangesResponse();
            if ((nowSaved.player1.Trim() == (string)(Session[Strings.USERNAME]) && nowSaved.code % 2 == 1) ||
                (nowSaved.player2.Trim() == (string)(Session[Strings.USERNAME]) && nowSaved.code % 2 == 0) &&
                postChangesReq.code == nowSaved.code)
            {
                updateGame.board = postChangesReq.board;
                updateGame.code = nowSaved.code + 1;
                gamesDB.Entry(nowSaved).CurrentValues.SetValues(updateGame);
                gamesDB.SaveChanges();
                resp.approved = true;
                resp.code = updateGame.code;
            }
            if (updateGame.player2.Trim() == "AI")
            {
                AI();
            }
            return Json(resp);
            //TODO
            //((Board)Session["board"]).RegisterMove(m);
            //Move m2 = ((Board)Session["board"]).RequestMove();
            //if(m2 != null)
            //    ((Board)Session["board"]).RegisterMove(m2);
            //return Content("S|" + m2.x + "|" + m2.y + "|" + ((Board)Session["board"]).getVictory());
        }

        public void AI()
        {
            TicTacToeDatabaseEntities gamesDB = new TicTacToeDatabaseEntities();
            List<Game> games = gamesDB.Games.ToList();
            Game lastGame = (from game in games where game.Id == (int)(Session[Strings.GAMEID]) select game).FirstOrDefault();

            char[] b = lastGame.board.Trim().ToCharArray();
            int ran = new Random().Next(1, b.Length);
            int counter = ran;
            bool done = false;
            while (!done && counter != 0)
            {
                counter--;
                for (int i = 0; i < b.Length; i++)
                {
                    if (b[i] == '0')
                    {
                        if (ran == 0)
                        {
                            done = true;
                            b[i] = '2';
                            break;
                        }
                        else
                        {
                            ran--;
                        }
                    }
                }
            }
            Game updateGame = new Game();
            updateGame.Id = lastGame.Id;
            updateGame.player1 = lastGame.player1;
            updateGame.player2 = lastGame.player2;


            updateGame.board = new string(b);
            updateGame.code = lastGame.code + 1;
            gamesDB.Entry(lastGame).CurrentValues.SetValues(updateGame);
            gamesDB.SaveChanges();


        }
    }
}
